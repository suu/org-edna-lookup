;;; org-edna-lookup.el --- Dependency visualizer for org-edna -*- lexical-binding: t -*-

;; Copyright (C) 2021 suu

;; Author: suu
;; Version : 0.2.1
;; Package-Requires: ((emacs "27.1") (dash "2.18") (org-edna "1.1.2"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; This file is not part of GNU Emacs.

;;; Commentary:
;; Builds a buffer that displays an org-mode heading's TRIGGER dependents and
;; BLOCKER dependencies.
;; Supported edna features:
;;
;; Local finder keywords (next-sibling, parent, children)
;; ids()
;;
;; Non-heading-visiting finders like "file()" are not currently supported.
;;
;; done? is the only BLOCKER condition currently supported.


(require 'org-edna)
(require 'ol)
(require 'org)
(require 'dash)

;;; Code:

(defvar org-edna-lookup-quit-action 'bury-buffer
  "Quit command for the *org-edna-lookup* buffer.
Could be 'bury-buffer, 'quit-window, 'kill-this-buffer, etc.")

(defun org-edna-lookup--exp-action-p (exp)
  "Predicate to determine whether a given expression EXP is an edna action.

\(todo! NEXT)  => t
\(!done?)      => nil
\(ids ...)     => nil"
  (fboundp (intern (concat "org-edna-action/" (symbol-name (car exp))))))

(defun org-edna-lookup--exp-finder-p (exp)
  "Predicate to determine whether the given expression EXP is an edna finder.

\(todo! NEXT)   => nil
\(ids ...)      => t
\(next-sibling) => t"
  (fboundp (intern (concat "org-edna-finder/" (symbol-name (car exp))))))

(defun org-edna-lookup--exp-heading-p (exp)
  "Predicate to determine whether the given expression EXP is an org-edna-lookup
heading reference; e.g. not an edna action, finder, or blocker."
  (and (not (string= (symbol-name (car exp)) "!done\?")) (not (or (org-edna-lookup--exp-action-p exp) (org-edna-lookup--exp-finder-p exp)))))

(defun org-edna-lookup--parse-forms (&optional heading-pos)
  "Parse the given heading, its dependents and dependencies into Lisp forms.
Use the org heading at HEADING-POS or `point' if nil."
  (let*  ((heading-pos (or heading-pos (point)))
          (blocker (org-entry-get heading-pos "BLOCKER" org-edna-use-inheritance))
          (trigger (org-entry-get heading-pos "TRIGGER" org-edna-use-inheritance))
          (blocker-sexp (org-edna-string-form-to-sexp-form blocker 'condition))
          (trigger-sexp (org-edna-string-form-to-sexp-form trigger 'action)))

    (save-excursion
      (goto-char heading-pos)

      `(:blockers
        ,(when blocker
           (--map (progn
                   (org-edna-lookup--exp-form-to-markers it))
                 blocker-sexp))
        :triggers
        ,(when trigger
           (--map (progn
                   (org-edna-lookup--exp-form-to-markers it))
                 trigger-sexp))))))

(defun org-edna-lookup--locate (edna-form)
  "Parse the edna finder group in EDNA-FORM into a marker."
  (let* ((finder (car edna-form))
         (args (cdr edna-form))
         (fn (intern (concat "org-edna-finder/" (symbol-name finder))))
         (markers (apply fn args)))
    (list edna-form markers)))

(defun org-edna-lookup--exp-form-to-markers (sexp-form)
  "Parse a set of edna forms SEXP-FORM into a plist of org headings and
associated edna actions/conditions.

Heading data is stored under the :headings key is a list of two lists.
The `car' is the finder group that the `cdr' headings alist was generated from.
\((finder) ((HEADING-TEXT . HEADING-ID) (HEADING-TEXT . HEADING-ID)))

Edna keywords are stored in a similar alist under the :keywords key."
  (let ((headings '())
        (ac-conds '()))
    (--each sexp-form
      (cond
       ((org-edna-lookup--exp-finder-p it)
        (add-to-list 'headings (org-edna-lookup--finder-to-props it)))
       (t
        (add-to-list 'ac-conds it))))

    `(:headings ,headings :keywords ,ac-conds)))

(defun org-edna-lookup--finder-to-props (form)
  "Convert an edna finder symbol form to a list of org heading pairs.
\(HEADING-TEXT HEADING-ID\)"
  (let* ((finder-group (org-edna-lookup--locate form))
         (markers (cadr finder-group))
         (fsym (car finder-group)))
    (list fsym
          (--map
           (progn
             (save-excursion
               (goto-char it)
               (list (substring-no-properties
                      (org-get-heading))
                     (org-entry-get (point) "ID"))))
           markers))))

(defun org-edna-lookup--form-to-string (sexp-form)
  "Convert an edna symbols alist in SEXP-FORM into a string.
\((todo! NEXT) (scheduled .)) => \"todo! NEXT scheduled .\"."
  (cond
   ((consp sexp-form)
    (apply #'concat
           (--map
            (progn
              (concat (string-trim (org-edna-lookup--form-to-string it)
                                   nil "")))
            sexp-form)))
   ((symbolp sexp-form)
    (concat (symbol-name sexp-form) " "))
   ((stringp sexp-form)
    (concat sexp-form " "))))

(defun org-edna-lookup--create-buffer ()
  "Create and perform initial setup of the view buffer."
  (let ((buf (generate-new-buffer "*org-edna-lookup*")))
    (with-current-buffer buf
      (org-mode)
      (org-edna-lookup-mode)
      (use-local-map org-edna-lookup-mode-map))
    buf))

(defun org-edna-lookup--clean-heading (heading)
  "Strip all test properties and org links from HEADING."
  (let*  ((strip-heading (substring-no-properties heading)))
    (if (string-match org-link-bracket-re strip-heading)
        (string-replace
         (match-string 0 strip-heading)
         (match-string 2 strip-heading) heading)
      strip-heading)))

(defun org-edna-lookup--buffer-insert-group (group)
  "Create a headings tree in GROUP for a level-2 group in the view buffer."
  (let* (
         (heading-groups (plist-get group :headings))
         (ac-conds (plist-get group :keywords))
         (group-label (org-edna-lookup--form-to-string ac-conds)))
    (insert "** " group-label "\n")
    (--each heading-groups
      (insert "*** " (org-edna-lookup--form-to-string (car it)) "\n")
      (--each (cadr it)
        (insert "**** " (org-edna-lookup--clean-heading (car it)) "\n")
        (let* ((plain-heading (substring-no-properties (org-get-heading t t t t))))
          (previous-line 2)
          (beginning-of-line)
          (when (search-forward plain-heading)
            (replace-match (concat "[[" "id:" (cadr it) "]["
                                   plain-heading
                                   "]]")))
          (end-of-line)
          (insert "\n"))))))

;;;###autoload
(defun org-edna-lookup ()
  "Parse, lookup, create, and display the view buffer."
  (interactive)
  (let* ((forms (org-edna-lookup--parse-forms))
         (blockers (plist-get forms :blockers))
         (triggers (plist-get forms :triggers))
         (buf (org-edna-lookup--create-buffer)))

    (when (and (not blockers) (not triggers))
      (user-error "Selected heading has no BLOCKER or TRIGGER properties."))

    (other-window 1)
    (switch-to-buffer buf)

    (with-current-buffer buf
      (insert (propertize "Linked Headings\n\n"
                          'face 'org-document-title 'font-lock-face
                          'org-document-title))
      (--each blockers
        (insert "* Blockers\n")
        (org-edna-lookup--buffer-insert-group (car blockers))
        (insert "\n\n"))

      (--each triggers
        (insert "* Triggers\n")
        (org-edna-lookup--buffer-insert-group (car triggers))
        (insert "\n\n"))

      (read-only-mode)

      (goto-char (point-min)))))

(defvar org-edna-lookup-mode-map
  (let ((kmap (make-sparse-keymap)))
    (define-key kmap (kbd "q") org-edna-lookup-quit-action)
    (define-key kmap (kbd "RET") #'org-open-at-point)
    (set-keymap-parent kmap org-mode-map)
    kmap))

(define-minor-mode org-edna-lookup-mode
  "View org-edna related tasks"
  :global nil
  :keymap org-edna-lookup-mode-map
  (use-local-map org-edna-lookup-mode-map))

(provide 'org-edna-lookup)

;;; org-edna-lookup.el ends here
